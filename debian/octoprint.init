#!/bin/sh
#
### BEGIN INIT INFO
# Provides:          octoprint
# Required-Start:    $local_fs $remote_fs $network
# Required-Stop:     $local_fs $remote_fs $network
# Should-Start:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Run octoprint
# Description:       Octoprint provides a responsive web interface for
#                    controlling a 3D printer
### END INIT INFO

# OctoPrint's run script
NAME=octoprint
DAEMON=/usr/bin/$NAME
DESC="Octoprint 3D printer web interface"

# Exit if the run script is not found
[ -x "$DAEMON" ] || exit 0

. /lib/lsb/init-functions

# Check if octoprint is configured or not
test -f "/etc/default/${NAME}" && . "/etc/default/${NAME}"

[ -z "$OCTOPRINT_PORT" ] && OCTOPRINT_PORT=5000
[ -z "$OCTOPRINT_USER" ] && OCTOPRINT_USER=nobody

check_run_dir() {
    # Create the run directory if necessary.
    if [ ! -d /var/run/octoprint ]; then
	mkdir -p /var/run/octoprint
	chmod 0755 /var/run/octoprint
	chown octoprint:octoprint /var/run/octoprint
    fi
}

case "$1" in
start)
	check_run_dir
	log_daemon_msg "Starting $DESC" "$NAME"
	su $OCTOPRINT_USER -c "$DAEMON --pid /var/run/octoprint/pid --port=$OCTOPRINT_PORT -b /var/lib/octoprint $OCTOPRINT_FLAGS --daemon start"
	log_end_msg $?
	;;
stop)
	log_daemon_msg "Stopping $DESC" "$NAME"
	su $OCTOPRINT_USER -c "$DAEMON --pid /var/run/octoprint/pid --port=$OCTOPRINT_PORT -b /var/lib/octoprint $OCTOPRINT_FLAGS --daemon stop"
	log_end_msg $?
	;;
restart|force-reload)
	check_run_dir
	log_daemon_msg "Restarting $DESC" "$NAME"
	su $OCTOPRINT_USER -c "$DAEMON --pid /var/run/octoprint/pid --port=$OCTOPRINT_PORT -b /var/lib/octoprint $OCTOPRINT_FLAGS --daemon restart"
	log_end_msg $?
	;;
status)
	if [ -f /var/run/octoprint/pid ] &&
	    kill -0 $(cat /var/run/octoprint/pid)
	then
		echo "Octoprint is running."
	else
		echo "Octoprint is stopped."
	fi
	;;
*)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
	;;
esac

# Fallthrough if work done.
exit 0
