Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OctoPrint
Source: https://github.com/foosel/OctoPrint

Files: *
Copyright: Gina Häußge <gina@foosel.net>
License: AGPL-3

Files: octoprint/static/font/fontawesome*
Copyright: 2012-2013 Dave Gandy <dave@davegandy.com>
License: OFL-1.1
Comment:
 Upstream doesn't specify which version of OFL instead simply links to
 OFL page. Hence latest version of OFL is considered.

Files: octoprint/static/gcodeviewer/lib/three.min.js
 octoprint/static/gcodeviewer/lib/three.js
Copyright: 2010-2013 Ricardo Cabello Miguel, Larry Battle
License: Expat

Files: octoprint/static/gcodeviewer/lib/TrackBallControls.js
Copyright: 2011-2013 Eberhard Gräther <mail@egraether.com>
License: Expat

Files: octoprint/static/gcodeviewer/css/cupertino/*
 octoprint/static/js/lib/jquery/*
Copyright: 2012 jQuery Foundation and other contributors.
License: Expat

Files: octoprint/static/js/lib/underscore.js
Copyright: 2009-2013, Jeremy Ashkenas <jeremy@documentcloud.org>
License: Expat

Files: octoprint/static/js/lib/avltree.js
Copyright: 2004-2006 Sébastien CRAMATTE <scramatte@zensoluciones.com>,
	2012 Jeremy Stephens <viking@pillageandplunder.net>
License: GPL-2+ and LGPL-2.1+

Files: octoprint/static/gcodeviewer/js/*
Copyright: 2012 Alex Ustyantsev <hudbrog@gmail.com>
License: AGPL-3

Files: octoprint/static/css/bootstrap*
 octoprint/static/js/lib/bootstrap/bootstrap.*
Copyright: 2012 Twitter, Inc.
License: Apache-2.0

Files: octoprint/static/css/bootstrap-modal.css
 octoprint/static/js/lib/bootstrap/bootstrap-modal*
Copyright: 2012 Jordan Schroter
License: Apache-2.0

Files: octoprint/static/css/font-awesome.min.css
Copyright: 2012-2013 Dave Gandy <dave@davegandy.com>
License: Expat

Files: octoprint/static/css/jquery.pnotify.default.css
Copyright: 2009 Hunter Perrin
License: Expat

Files: octoprint/static/css/jquery.fileupload-ui.css
Copyright: 2010 Sebastian Tschan <mail@blueimp.net>
License: Expat

Files: octoprint/util/gcodeInterpreter.py
Copyright: 2013 David Braam <daid303@gmail.com>
License: AGPL-3

Files: debian/*
Copyright: 2013 Tonnerre Lombard <tonnerre@ancient-solutions.com>
License: GPL-2+

License: AGPL-3
 Copyright (c) 2011-2013, Gina Häußge <gina@foosel.net>
 .
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 See /usr/share/common-licenses/LGPL-2 for the full license text.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian GNU/Linux systems, the complete text of the Apache 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.

License: OFL-1.1
 PREAMBLE
  The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font creation
 efforts of academic and linguistic communities, and to provide a free and
 open framework in which fonts may be shared and improved in partnership
 with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded, 
 redistributed and/or sold with any software provided that any reserved
 names are not used by derivative works. The fonts and derivatives,
 however, cannot be released under any other type of license. The
 requirement for fonts to remain under this license does not apply
 to any document created using the fonts or their derivatives.
 .
 DEFINITIONS
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software components as
 distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to, deleting,
 or substituting -- in part or in whole -- any of the components of the
 Original Version, by changing formats or by porting the Font Software to a
 new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
  Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed, modify,
 redistribute, and sell modified and unmodified copies of the Font
 Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
    in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
    redistributed and/or sold with any software, provided that each copy
    contains the above copyright notice and this license. These can be
    included either as stand-alone text files, human-readable headers or
    in the appropriate machine-readable metadata fields within text or
    binary files as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
    Name(s) unless explicit written permission is granted by the corresponding
    Copyright Holder. This restriction only applies to the primary font
    name as presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
    Software shall not be used to promote, endorse or advertise any
    Modified Version, except to acknowledge the contribution(s) of the
    Copyright Holder(s) and the Author(s) or with their explicit written
    permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
    must be distributed entirely under this license, and must not be
    distributed under any other license. The requirement for fonts to
    remain under this license does not apply to any document created
    using the Font Software.
 .
 TERMINATION
  This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
  THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.
